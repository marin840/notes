package com.example.notesaplication

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast

class NotesAdapter: RecyclerView.Adapter<NotesAdapter.ViewHolder> (){

    class ViewHolder(itemView: View, var index: Int): RecyclerView.ViewHolder(itemView) {
        var noteTitleTextView: TextView? = null
        var noteDateTextView: TextView? = null

        init {
            noteTitleTextView = itemView.findViewById(R.id.noteTitleTextView)
            noteDateTextView = itemView.findViewById(R.id.noteDateTextView)

            itemView.setOnClickListener{
                val intent = Intent(itemView.context, NoteDetailsActivity::class.java)
                intent.putExtra("noteIndex", index)
                intent.putExtra("note", "existingNote")
                itemView.context.startActivity(intent)
            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val notesListElement = inflater.inflate(R.layout.note_title, parent, false)
        return ViewHolder(notesListElement, position)

    }

    override fun getItemCount(): Int {
        return NotesList.notesList.size
    }

    override fun onBindViewHolder(parent: ViewHolder, position: Int) {
        parent.noteTitleTextView?.text = NotesList.notesList[position].noteTitle
        parent.noteDateTextView?.text = NotesList.notesList[position].noteDateTime
        parent.index = position

    }
}