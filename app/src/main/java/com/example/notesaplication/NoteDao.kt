package com.example.notesaplication

import android.arch.persistence.room.*

@Dao
interface NoteDao {

    @Insert
    fun insertAll(vararg notes: Note)

    @Delete
    fun deleteNote(vararg notes: Note)

    @Update
    fun updateNote(vararg notes: Note)

    @Query("SELECT * FROM Note")
    fun getAllNotes() : MutableList<Note>
}