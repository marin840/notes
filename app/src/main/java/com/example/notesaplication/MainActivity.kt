package com.example.notesaplication

import android.arch.persistence.room.Room
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.example.notesaplication.NotesList.notesList
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var notesAdapter: NotesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val db = Room.databaseBuilder(
            applicationContext,
            NoteDatabase::class.java, "notes_database"
        ).build()
        NotesDB.databaseReference = db
        Thread(Runnable {
            NotesList.notesList = NotesDB.databaseReference!!.noteDao().getAllNotes().reversed().toMutableList()
        }).start()

        listOfNotesRecycleView.layoutManager = LinearLayoutManager(this)
        notesAdapter = NotesAdapter()
        listOfNotesRecycleView.adapter = notesAdapter

        floatingActionButton.setOnClickListener{
            val startNoteDetailsActivityIntent = Intent(this, NoteDetailsActivity::class.java)
            startNoteDetailsActivityIntent.putExtra("noteIndex", NotesList.notesList.count())
            startNoteDetailsActivityIntent.putExtra("note", "newNote")
            startActivity(startNoteDetailsActivityIntent)
        }


    }

    override fun onResume(){
        notesAdapter.notifyDataSetChanged()
        super.onResume()
    }
}
