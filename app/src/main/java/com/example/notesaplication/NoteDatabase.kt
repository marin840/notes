package com.example.notesaplication

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database (entities = arrayOf(Note::class), version = 1)
abstract class NoteDatabase: RoomDatabase() {
    abstract fun noteDao(): NoteDao
}