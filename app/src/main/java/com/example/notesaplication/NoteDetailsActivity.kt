package com.example.notesaplication

import android.arch.persistence.room.Room
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.notesaplication.NotesList.notesList
import  kotlinx.android.synthetic.main.activity_note_details.*
import kotlinx.android.synthetic.main.note_title.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class NoteDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note_details)


        var index = intent.getIntExtra("noteIndex", -1)
        var noteMessage = intent.getStringExtra("note")

        if(noteMessage.equals("existingNote")){
            noteDescriptionEditText.setText(NotesList.notesList[index].noteDescription)
            notesTitleEditText.setText(NotesList.notesList[index].noteTitle)
        }

        saveButton.setOnClickListener{

            val current = java.util.Calendar.getInstance().time
            val parts = current.toString().split(" ")
            val dateTime = parts[1] + ' ' + parts[2] + ' ' + parts[3]

            if(noteMessage.equals("newNote")) {

                var note = Note(0)
                note.noteDescription = noteDescriptionEditText.text.toString()
                note.noteTitle = notesTitleEditText.text.toString()


                //val noteDate = SimpleDateFormat("dd.M.yyyy").format(Date())

                note.noteDateTime = dateTime

                NotesList.notesList.add(note)

                Thread(Runnable {
                    NotesDB.databaseReference!!.noteDao().insertAll(note)
                    NotesList.notesList = NotesDB.databaseReference!!.noteDao().getAllNotes().reversed().toMutableList()
                }).start()

                //Toast.makeText(this, noteDate.toString(), Toast.LENGTH_LONG).show()
            }
            else {
                NotesList.notesList[index].noteDateTime = dateTime
                NotesList.notesList[index].noteDescription = noteDescriptionEditText.text.toString()
                NotesList.notesList[index].noteTitle = notesTitleEditText.text.toString()

                Thread(Runnable {
                    NotesDB.databaseReference!!.noteDao().updateNote(notesList[index])
                    NotesList.notesList = NotesDB.databaseReference!!.noteDao().getAllNotes().reversed().toMutableList()
                }).start()
            }

            finish()
        }

        deleteButton.setOnClickListener{
            var noteToDelete = NotesList.notesList[index]
            if(!noteMessage.equals("newNote"))
                NotesList.notesList.removeAt(index)
            Thread(Runnable {
                NotesDB.databaseReference!!.noteDao().deleteNote(noteToDelete)
            }).start()
            finish()
        }
    }
}
