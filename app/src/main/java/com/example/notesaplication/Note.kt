package com.example.notesaplication

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.util.*

@Entity
data class Note(

    @PrimaryKey(autoGenerate = true) var pid : Int,
    @ColumnInfo(name = "note_description") var noteDescription: String? = null,
    @ColumnInfo(name = "note_title") var noteTitle: String? = null,
    @ColumnInfo(name = "note_datetime") var noteDateTime : String? = null

)